# Templates

Templates available for all projects of this group.

 More explanations in [Gitlab documentation](https://docs.gitlab.com/ee/user/admin_area/settings/instance_template_repository.html#instance-template-repository-premium-only)

## Development and testing

1. Find a project using template
1. Create a testing branch
1. Reference the `templates` branch you're working on in the test branch `.gitlab-ci.yml` using `ref` keyword

```yaml
include:
  - project: dolmen-public/templates
    ref: reference # Set your branch name here
    file: '/templates/.gitlab-ci-template.yml'
```

1. After tests, remove testing branch without merging it
